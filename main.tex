% Font size
\documentclass[12pt]{article}

% utf8 support
\usepackage[utf8]{inputenc}

\usepackage{hyperref}

% Referencing
\usepackage[backend=biber,style=apa]{biblatex}
\addbibresource{references.bib}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr}

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\textbf{Critical Appraisal of Regan, 1995}}

\author{\textit{2270405d}}

 % Date, use \date{} for no date
\date{\today}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontol header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{\textbf{Quantitative Methods critical appraisal}}
% right hand side header
\rhead{\docauthor}


% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title section

%----------------------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%----------------------------------------------------------------------------------------

%\renewcommand{\abstractname}{Summary} % Uncomment to change the name of the abstract to something else

%\begin{abstract}
%	abstract
%\end{abstract}

\hspace*{3.6mm}\textit{Keywords:} critical appraisal, Regan, US, economic assistance% Keywords

\vspace{30pt} % Vertical whitespace between the abstract and first section

%----------------------------------------------------------------------------------------
%	ESSAY BODY
%----------------------------------------------------------------------------------------

\section*{}

This essay will critically appraise \textcite[]{regan_us_1995}, a journal article that seeks to evaluate “the impact of changes in US economic assistance on changes in political abuse” \autocite[613]{regan_us_1995} through quantitative analysis. It will first summarise the paper in order to then evaluate whether its quantitative study answers the hypothesis formulated from a contexualized literature review. This essay will then show that while Regan did correctly identify a needed research avenue, he only confirmed his null hypothesis through a data-set and a methodology that both presented problematic flaws.

\section*{}

First, Regan explores the prescriptions of US policy on foreign aid. He highlights the different perspectives on Human Rights of the Reagan and Carter US administrations: the former’s tenure put less emphasis on these rights. In congress, foreign assistance is used to both promote Human Rights and distance the US from offenders. Regan points out that the congress’ and the administrations’ attitudes reveal a concensus on the existence of a causal relationship between levels of aid and levels of repression.

After reviewing past literature, Regan highlights two research trends which explore this link. One argues that Human Rights practices affect amounts of foreign aid, whereas the other contends that levels of foreign aid affect respect for Human Rights. Since past research has focused on the former, Regan has chosen to focus on the latter. Moreover, past studies have analyzed levels of repression, not changes in levels of repression, therefore, he will explore the latter.

Regan notes that methodological problems in measures of political repression come from weak sources of data or too few cases to construct a model. His work consists in analyzing one data-set from Amnesty international, focusing on five categories of repression across 328 cases coded by level of repression on a numerical scale. Regan crosses this data with the US’ development coordination committee’s economic assistance data, military assistance data from the Correlates of War Material Capabilities data set and data on democracy from the Polity II data set.

Regan summarises that “U.S. economic aid has had little or no impact on the Human Rights practices of the recipient governments” \autocite[625]{regan_us_1995} rather, economic aid serves as a diplomatic message. He concludes by setting a heading for future research, noting that the field needs a “multivariate model predicting changes in repression” “that might guide future empirical analysis” \autocite[626]{regan_us_1995}.

\section*{}

Regan conveys a well-structured analysis of the causal link between foreign aid and Political repression. His introduction contextualizes the paper in its current research domain, swiftly followed by a thorough review of the literature on the subject. Regan then introduces the research methodology and related data sources before conducting a thorough review of his findings. The article concludes on a brief albeit complete summary of his research and where it fits in the research domain before explaining how his work could be furthered.

Beyond the attention Regan has given to the cohesion of his writing, conducting a literature review before a quantitative analysis is particularly relevant for integrating his analysis into the field. Moreover, the review allowed Regan to spot a previously unexplored research avenue: “turning the putative causal equation around to examine the effect of aid disbursements on Human Rights violations” \autocite[613]{regan_us_1995}. Another innovation in the domain is the focus on changes in aid levels rather than aid levels themselves, which better reveals the causal relation between aid and repression by emphasizing the cause and effect rather than a mere link between the two.

\section*{}

Regan’s data-set was the best available at the time \autocite{poe_repression_1994}, however this does not emancipate it from its weaknesses. The time period (1977-88) covered is too short for long term analysis \autocite{poe_repression_1994,regan_us_1995}. To Regan’s credit, he does acknowledge this by claiming that “this time frame is still insufficient to permit reliable longitudinal analysis” \autocite[619]{regan_us_1995}.

As Regan states, the data covers 32 latin american and asian countries, which excludes any African nations. However, the causal relation between US economic aid and repression has been demonstrated to also apply to African countries \autocite{poe_human_1993}. Even if this was not the case, data on these countries should have been included when generally studying the effect of US foreign aid on “political repression”.

These data set issues could have been mitigated had Regan based his analysis on more than one data-set. As he states himself, the reliability and availability of data on Human Rights abuses is generally low \autocite[619]{regan_us_1995}, this is also widely confirmed by other literature in the field \autocite{carleton_role_1987,poe_repression_1994,schoultz_u._1981,stohl_human_1984}. However, Regan acknowledges the existence of another data set which he has apparently partially analyzed. Indeed, he calculated a correlation score with the US state department’s reports on Human Rights from the period, as well as Carleton and Stohl, 1987‘s data \autocite[620]{regan_us_1995}. His justification for only basing his analysis on Amnesty International’s data is due to “the confusing nature of presenting two complete sets of findings” \autocite[620]{regan_us_1995} as well as bias in the other data source.

The first argument seems out of place, especially given that it has been recommended to compare the findings from both data sources \autocite{carleton_role_1987}. Considering the scarcity of data and Regan’s own acknowledgment of the limitations of all sources in this field \autocite[619]{regan_us_1995}, it is bewildering that he omitted extra sources of data simply because it would be “confusing”. It is precisely the role of the demonstrator to shed light on such confusion, moreover, other articles have conducted their analysis with multiple sources of data (these specific ones) without “confusion” \autocite{carleton_role_1987,poe_human_1992,poe_repression_1994,stohl_human_1984}.

Concerning the bias of the US state department’s data, other literature \autocite{carleton_role_1987,poe_human_1993,poe_repression_1994} has included both data sources while acknowledging the bias in the analysis to compare the results from both data sets. Moreover, Regan had other reliable sources of data at his disposal: the civil and political rights scales of the Freedom House Organization is one example that was available at the time \autocite{carleton_role_1987,poe_human_1993,poe_repression_1994,stohl_human_1984}.

\section*{}

Regan’s five categories of repression are another disputable point of his analysis as they focus on “the most dramatic form of repression--repression of personal integrity rights” \autocite[853]{poe_repression_1994}. They consist of “disappearances, torture, arbitrary arrests, political prisoners and political killings” \autocite[620]{regan_us_1995}. Hence, forms of repression which do not harshly and physically target individuals are excluded, therefore misrepresenting political repression. The main criticism here is that Regan seeks to establish a measure of levels of political repression based on a framework from Poe and Tate, 1994 that was originally used to “focus on the subset of Human Rights categorized as dealing with the integrity of the person” \autocite[854]{poe_repression_1994} rather than to draw conclusions on political repression as a whole. Regan should have included other forms of repression such as those mentionned in the literature he cites: denial of fair public trial, invasion of the home degrading punishments \autocite{cingranelli_human_1985}. He could then have factored these civil and political rights into his repression score like most research has tended to do \autocite{carleton_role_1987,cingranelli_human_1985,mccormick_is_1988,poe_human_1993}.

Lastly, another omission by Regan is the notion of “Gate-Keeping”, a term coined by past papers in this domain of study \autocite{cingranelli_human_1985,poe_human_1992,poe_human_1993}. In essence, the decision of foreign aid allocation is a two-stage process, first the decision of whether to allocate any foreign aid is taken, then, the amount of foreign aid is decided. Gate-keeping occurs when “some countries [are] systematically excluded from the recipient pool” \autocite[148]{poe_human_1992}. Regan does not differentiate complete cessations of foreign aid from decreases in allocation. Considering Regan’s qualification of foreign aid as “an indicator of a political message being sent by the administrators” \autocite[614]{regan_us_1995} this distinction is important. Notably because decreases in foreign aid are an altogether different “political message” than a non-allocation of foreign aid.

\section*{}

Overall, Regan’s article fits particularly well in its research landscape. As he demonstrates in his literature review, plenty of research has studied the effects of Human Rights abuses on US foreign policy, yet none have turned the causal equation around. Hence, Regan focuses on the effects of changes in US economic aid on changes in human rights abuses, in essence: does allocation of aid actually impact Human Rights practices? Regan’s quantitative analysis of Amnesty International’s data confirms his null hypothesis: US foreign aid has little to no impact on the recipient’s Human Rights practices.

Despite being aptly worded, contextualized and targeted at a particular need for research, Regan’s work has major flaws. Firstly, Amnesty’s data-set covers a short time period and a few, localized countries, limiting findings to the data’s temporal and geographical context. Secondly, Regan knowingly omits data sources, this is particularly troublesome in lieux of the first point regarding the limitations of the data used. Thirdly, Regan applies a framework, from another article for measuring a subset human rights, to his study of political repression as a whole. Lastly, Regan does not differentiate a decline in foreign aid from a cessation, overlooking an important aspect of the allocation process that past literature has dubbed “Gate-keeping”.

According to this essay, the direction taken and the point formulated by Regan’s article are particularly interesting and relevant to the field of study. However, this article is not cautious enough in drawing swift and generalizing conclusions from a single report with a fundamental data quality problem alongside methodical flaws in the quantitative analysis.

\newpage

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\printbibliography

%----------------------------------------------------------------------------------------

\end{document}
